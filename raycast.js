const BLOCK_SIZE = 32;
const MAP_WIDTH = 18;
const MAP_HEIGHT = 20;
const WINDOW_WIDTH = MAP_WIDTH*BLOCK_SIZE;
const WINDOW_HEIGHT = MAP_HEIGHT*BLOCK_SIZE;
const PI = Math.PI;

const COL_RESOLUTION = 10; //WALL pixel width
const FOV_ANGLE = 60*(PI/180);
const NUM_RAYS = WINDOW_WIDTH/COL_RESOLUTION;

class Map{
	constructor() {
		this.grid = [
			[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
			[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1],
			[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,1,1,1,1,1,0,0,1,0,0,1,0,0,1,0,0,1],
			[1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1],
			[1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,1,0,0,1,1,0,1,1,1,0,0,1,1],
			[1,0,0,0,0,1,0,0,1,0,0,0,1,0,0,0,0,1],
			[1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1],
			[1,0,0,0,0,1,0,0,1,0,0,0,1,0,0,0,0,1],
			[1,0,0,0,0,1,0,0,1,0,0,0,1,0,0,0,0,1],
			[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
		];
	}

	render(){
		for (var i = 0; i < MAP_HEIGHT; i++) {
			for (var j = 0; j < MAP_WIDTH; j++) {
				var tileX = j*BLOCK_SIZE;
				var tileY = i*BLOCK_SIZE;
				var tileColor = this.grid[i][j] == 1 ? "#222" : "#fff";
				stroke("#222");
				fill(tileColor);
				rect(tileX,tileY,BLOCK_SIZE,BLOCK_SIZE);
			}
		}
	}
	hasWallAt(x, y) {
        if (x < 0 || x > WINDOW_WIDTH || y < 0 || y > WINDOW_HEIGHT) {
            return true;
        }
        var mapGridIndexX = Math.floor(x / BLOCK_SIZE);
        var mapGridIndexY = Math.floor(y / BLOCK_SIZE);
        return (this.grid[mapGridIndexY][mapGridIndexX] != 0);
    }
}

class Ray{
	constructor(rayAngle){
		this.rayAngle = normalizeAngle(rayAngle);
		this.wallHitX = 0;
		this.wallHitY = 0;
		this.distance = 0;
		this.wasHitVertical = false;

		this.isRayFacingDown = this.rayAngle > 0 && this.rayAngle < PI;
		this.isRayFacingUp = !this.isRayFacingDown;

		this.isRayFacingRight = this.rayAngle < 0.5 * PI || this.rayAngle > 1.5 * PI;
		this.isRayFacingLeft = !this.isRayFacingRight;

	}
	cast(columnId){
		var xintercept, yintercept;
		var xstep, ystep;
		
		///////////////////////////////////////////////////////
		//////				Horizontal intersection
		///////////////////////////////////////////////////////
		var foundHorizontalWallHit = false;
		var horizontalWallHitX = 0;
		var horizontalWallHitY = 0;

		// horizontal intersection
		yintercept = Math.floor(player.posY/BLOCK_SIZE)*BLOCK_SIZE;
		yintercept += this.isRayFacingDown ? BLOCK_SIZE : 0;

		xintercept = player.posX+(yintercept - player.posY)/Math.tan(this.rayAngle);

		ystep = BLOCK_SIZE;
		ystep *= this.isRayFacingUp ? -1 : 1;
		xstep = BLOCK_SIZE/Math.tan(this.rayAngle);
		xstep *= (this.isRayFacingLeft && xstep > 0) ? -1 : 1;
		xstep *= (this.isRayFacingRight && xstep < 0) ? -1 : 1;

		var nextHorizontalTouchX = xintercept;
		var nextHorizontalTouchY = yintercept;

		/*if (this.isRayFacingUp){
			nextHorizontalTouchY--;
		}*/
			
		while(nextHorizontalTouchX >= 0 &&
		 nextHorizontalTouchX <= WINDOW_WIDTH
		  && nextHorizontalTouchY >= 0 &&
		   nextHorizontalTouchY <= WINDOW_HEIGHT){
			if (level.hasWallAt(nextHorizontalTouchX, nextHorizontalTouchY - (this.isRayFacingUp ? 1 : 0))){
				foundHorizontalWallHit = true;
				horizontalWallHitX = nextHorizontalTouchX;
				horizontalWallHitY = nextHorizontalTouchY;

				/*stroke("red");
				line(player.posX, player.posY, horizontalWallHitX, horizontalWallHitY)
				*/
				break;
			} else {
				nextHorizontalTouchX += xstep;
				nextHorizontalTouchY += ystep;
			}
		}


		///////////////////////////////////////////////////////
		//////				Vertical intersection
		///////////////////////////////////////////////////////
		var foundVerticalWallHit = false;
		var verticalWallHitX = 0;
		var verticalWallHitY = 0;

		// vertical intersection
		xintercept = Math.floor(player.posX/BLOCK_SIZE)*BLOCK_SIZE;
		xintercept += this.isRayFacingRight ? BLOCK_SIZE : 0;

		yintercept = player.posY+(xintercept - player.posX)*Math.tan(this.rayAngle);

		xstep = BLOCK_SIZE;
		xstep *= this.isRayFacingLeft ? -1 : 1;
		ystep = BLOCK_SIZE*Math.tan(this.rayAngle);
		ystep *= (this.isRayFacingUp && ystep > 0) ? -1 : 1;
		ystep *= (this.isRayFacingDown && ystep < 0) ? -1 : 1;

		var nextVerticalTouchX = xintercept;
		var nextVerticalTouchY = yintercept;

		/*if (this.isRayFacingLeft){
			nextVerticalTouchX--;
		}*/
			
		while(nextVerticalTouchX >= 0 &&
		 nextVerticalTouchX <= WINDOW_WIDTH
		  && nextVerticalTouchY >= 0 &&
		   nextVerticalTouchY <= WINDOW_HEIGHT){
			if (level.hasWallAt(nextVerticalTouchX - (this.isRayFacingLeft ? 1 : 0), nextVerticalTouchY)){
				foundVerticalWallHit = true;
				verticalWallHitX = nextVerticalTouchX;
				verticalWallHitY = nextVerticalTouchY;

				break;
			} else {
				nextVerticalTouchX += xstep;
				nextVerticalTouchY += ystep;
			}
		}
		///////////////////////////////////////////////////////
		//////				Calculate Distances
		///////////////////////////////////////////////////////
		var horizontalHitDistance = (foundHorizontalWallHit) 
		? DistanceBetweenPoints(player.posX, player.posY, horizontalWallHitX, horizontalWallHitY)
		: Number.MAX_VALUE;

		var verticalHitDistance = (foundVerticalWallHit)
		? DistanceBetweenPoints(player.posX, player.posY, verticalWallHitX, verticalWallHitY)
		: Number.MAX_VALUE;

		this.wallHitX = (horizontalHitDistance < verticalHitDistance) ? horizontalWallHitX : verticalWallHitX;
		this.wallHitY = (horizontalHitDistance < verticalHitDistance) ? horizontalWallHitY : verticalWallHitY;
		this.distance = (horizontalHitDistance < verticalHitDistance) ? horizontalHitDistance : verticalHitDistance;

		this.wasHitVertical = (verticalHitDistance < horizontalHitDistance);

	}
	render(){
		stroke("red");
		line(player.posX, player.posY,
			this.wallHitX, this.wallHitY
		);
		stroke(0,240,0,50)
		line(player.posX, player.posY,
			player.posX+Math.cos(this.rayAngle)*60,
			player.posY+Math.sin(this.rayAngle)*60
		);
	}
}

class Player{
	constructor(){
		this.posX = 2+(15*BLOCK_SIZE);
		this.posY = 2+(18*BLOCK_SIZE);
		this.radius = 8;
		this.walkDirection = 0;
		this.turnDirection = 0;
		this.rotationAngle = (3*PI)/2;
		this.moveSpeed = 2;
		this.rotationSpeed = 2*(PI/180);
	}
	tick(){
		this.rotationAngle += this.turnDirection * this.rotationSpeed;

		var moveStep = this.walkDirection*this.moveSpeed;
		var nextX = this.posX+Math.cos(this.rotationAngle)*moveStep;
		var nextY = this.posY+Math.sin(this.rotationAngle)*moveStep;
		if (!level.hasWallAt(nextX, nextY)) {
			this.posX = nextX;
			this.posY = nextY;
		}
	}
	render(){
		noStroke();
		fill("red");
		circle(this.posX,this.posY,this.radius);
		stroke("blue");
		line(this.posX, this.posY, this.posX+Math.cos(this.rotationAngle)*30, this.posY+Math.sin(this.rotationAngle)*30);
	}
}

var level = new Map();
var rays = [];
var player = new Player();

function normalizeAngle(angle){
	angle = angle % (2*PI);
	if (angle<0){
		angle=(2*PI)+angle;
	}
	return angle;
}

function DistanceBetweenPoints(x1, y1, x2, y2){ 
	return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}

function keyPressed(){
	if (keyCode == UP_ARROW){
		player.walkDirection = 1;
	} else if (keyCode == DOWN_ARROW){
		player.walkDirection = -1;
	} else if (keyCode == RIGHT_ARROW){
		player.turnDirection = 1;
	} else if (keyCode == LEFT_ARROW){
		player.turnDirection = -1;
	}
}

function keyReleased(){
		if (keyCode == UP_ARROW){
		player.walkDirection = 0;
	} else if (keyCode == DOWN_ARROW){
		player.walkDirection = 0;
	} else if (keyCode == RIGHT_ARROW){
		player.turnDirection = 0;
	} else if (keyCode == LEFT_ARROW){
		player.turnDirection = 0;
	}
}

function castAllRays(){
	var columnId = 0;

	var rayAngle = player.rotationAngle - (FOV_ANGLE/2);

	rays = [];
	for (var i = 0; i < NUM_RAYS; i++) {
		var ray = new Ray(rayAngle);
		ray.cast(columnId);
		rays.push(ray);

		rayAngle += FOV_ANGLE/NUM_RAYS;

		columnId++;
	}
}

function setup(){
	createCanvas(WINDOW_WIDTH,WINDOW_HEIGHT);
}

function eventTick(){
	player.tick();
	castAllRays();
	//console.log(player.turnDirection, player.walkDirection);
}

function draw(){
	eventTick();
	level.render();
	for (ray of rays) {
		ray.render();
	}
	player.render();
}